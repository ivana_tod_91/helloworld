public class Osoba {
    public String ime;

    public Osoba() {
    }

    public Osoba(String ime) {
        this.ime = ime;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
